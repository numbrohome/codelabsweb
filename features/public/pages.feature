#Feature: Static pages and home page.
#  In order to have access to certain static pages
#  As an unauthenticated user
#  I want to see the home page and other static pages
#
#  Background: The user is not logged in
#    Given a user is not logged in
#
#  Scenario: User can access the home page
#    Given a user is on the home page
#    When he visits the homepage
#    Then he should be on the home page