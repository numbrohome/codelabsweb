Feature: Authentication system.
  In order to create and destroy user session
  As an unauthenticated user
  I want to be able to logged in and logged out

  Background: The user is not logged in
    Given a user is not logged in

  Scenario: User can logged in to his account
    Given a user has an account
    When he logs in
    Then he should be on the dashboard page

  #Scenario: User can logged out
  #  Given a user is logged in
  #  When he logs out
  #  Then he should be on the home page