module UserHelper
  @default_user = {
      email:  'inf.marcinkowski@gmail.com',
      password: 'testtest'
  }

  def default_user
    @default_user
  end

  def login(name, password)
    visit('/login')
    fill_in('public_session_email', with: name)
    fill_in('public_session_password', with: password)
    click_button('login')
  end

  def create_user(attributes={})
    #TODO Use FactoryGirl
    user = Public::User.new({
      gender: Admin::User.gender_types(:male),
      birth_date: '1988-11-10',
      first_name: 'Marcin',
      last_name: 'Marcinkowski',
      country_id: 'PL'
    }.merge(default_user).merge(attributes))

    user.ip = '127.0.0.1'
    user.role = :admin
    user.status = Public::User.status_types(:enabled)
    user.save
  end
end

World(UserHelper)