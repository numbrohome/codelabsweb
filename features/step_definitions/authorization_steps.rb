Given(/^a user is not logged in$/) do
  visit('/en/')
  if find('#navigation').has_content?('logout')
    fail 'User is logged in.'
  end
end

Given(/^a user is logged in$/) do
  visit('/en/')
  if find('#navigation').has_content?('login')
    fail 'User is logged out.'
  end
end

When(/^he logs out$/) do
  pending # express the regexp above with the code you wish you had
end

When(/^he logs in$/) do
  user = default_user
  visit('/en/login')
  login(user[:email], user[:password])
end