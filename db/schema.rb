# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20121206211453) do

  create_table "cart", force: true do |t|
    t.integer  "user_id"
    t.decimal  "net_price",   precision: 9, scale: 2, null: false
    t.decimal  "gross_price", precision: 9, scale: 2, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at"
  end

  add_index "cart", ["user_id"], name: "fk_cart_user1_idx", using: :btree

  create_table "cart_item", force: true do |t|
    t.integer  "cart_id",                                                     null: false
    t.string   "name",        limit: 128,                                     null: false
    t.decimal  "net_price",               precision: 9, scale: 2,             null: false
    t.decimal  "gross_price",             precision: 9, scale: 2,             null: false
    t.integer  "quantity",                                        default: 1
    t.string   "link"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at"
  end

  add_index "cart_item", ["cart_id"], name: "fk_cart_item_cart1_idx", using: :btree

  create_table "category", force: true do |t|
    t.string "name", limit: 100, null: false
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "country", force: true do |t|
    t.string  "name",             limit: 32,                         null: false
    t.decimal "tax",                         precision: 4, scale: 2, null: false
    t.string  "code",             limit: 2,                          null: false
    t.string  "currency_code",    limit: 3,                          null: false
    t.string  "currency_symbol",  limit: 8,                          null: false
    t.string  "payment_provider", limit: 10,                         null: false
    t.string  "language",         limit: 2,                          null: false
  end

  create_table "course", force: true do |t|
    t.integer  "country_id",                                              null: false
    t.integer  "user_id",                                                 null: false
    t.integer  "category_id",                                             null: false
    t.string   "title",               limit: 128,                         null: false
    t.text     "text"
    t.text     "short_text"
    t.decimal  "price",                           precision: 8, scale: 2, null: false
    t.integer  "status",              limit: 1,                           null: false
    t.integer  "level",               limit: 1,                           null: false
    t.integer  "subject",             limit: 1,                           null: false
    t.integer  "duration"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
    t.string   "cover_file_name"
    t.string   "cover_content_type"
    t.integer  "cover_file_size"
    t.datetime "cover_updated_at"
  end

  add_index "course", ["category_id"], name: "index_course_category", using: :btree
  add_index "course", ["country_id"], name: "index_course_country", using: :btree
  add_index "course", ["user_id"], name: "index_course_user", using: :btree

  create_table "entry", force: true do |t|
    t.integer  "user_id",                                     null: false
    t.integer  "country_id",                                  null: false
    t.string   "title",               limit: 128,             null: false
    t.integer  "status",              limit: 1,               null: false
    t.integer  "main_display_mode",   limit: 1,               null: false
    t.text     "text",                                        null: false
    t.text     "short_text"
    t.integer  "hits",                            default: 0
    t.text     "code"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "banner_file_name"
    t.string   "banner_content_type"
    t.integer  "banner_file_size"
    t.datetime "banner_updated_at"
  end

  add_index "entry", ["country_id"], name: "index_entry_country", using: :btree
  add_index "entry", ["user_id"], name: "index_entry_user", using: :btree

  create_table "lesson", force: true do |t|
    t.integer  "course_id",                                           null: false
    t.integer  "user_id",                                             null: false
    t.string   "title",           limit: 128,                         null: false
    t.text     "text"
    t.text     "short_text"
    t.decimal  "price",                       precision: 4, scale: 2, null: false
    t.integer  "time",                                                null: false
    t.string   "content_url"
    t.string   "example_url"
    t.integer  "type_of_content", limit: 1,                           null: false
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at"
  end

  add_index "lesson", ["course_id"], name: "index_lesson_course", using: :btree
  add_index "lesson", ["user_id"], name: "index_lesson_user", using: :btree

  create_table "order", force: true do |t|
    t.integer  "user_id"
    t.integer  "cart_id",                    null: false
    t.integer  "status",           limit: 1, null: false
    t.integer  "download_counter"
    t.datetime "expired_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at"
  end

  add_index "order", ["cart_id"], name: "fk_order_cart1_idx", using: :btree
  add_index "order", ["user_id"], name: "index_purchase_user", using: :btree

  create_table "payment", force: true do |t|
    t.integer  "order_id",                                             null: false
    t.string   "external_id",      limit: 128
    t.string   "payment_provider", limit: 10,                          null: false
    t.integer  "status",           limit: 1
    t.decimal  "amount",                       precision: 9, scale: 2, null: false
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at"
  end

  add_index "payment", ["order_id"], name: "fk_payment_order1_idx", using: :btree

  create_table "user", force: true do |t|
    t.string   "provider"
    t.string   "provider_id",                     limit: 45
    t.string   "email",                           limit: 80
    t.string   "crypted_password",                                        null: false
    t.string   "salt",                            limit: 32,              null: false
    t.string   "address",                         limit: 128
    t.string   "location",                        limit: 32
    t.string   "zip_code",                        limit: 10
    t.string   "country_id",                      limit: 2
    t.string   "website",                         limit: 100
    t.string   "phone_number",                    limit: 20
    t.string   "role",                            limit: 30,              null: false
    t.string   "first_name",                      limit: 30
    t.string   "last_name",                       limit: 30
    t.integer  "status",                          limit: 1,               null: false
    t.string   "gender",                          limit: 1,               null: false
    t.string   "ip",                              limit: 32
    t.date     "birth_date",                                              null: false
    t.string   "encrypted_new_email"
    t.string   "identification_token",            limit: 64
    t.string   "remember_me_token",               limit: 64
    t.datetime "remember_me_token_expires_at"
    t.string   "removal_token",                   limit: 64
    t.string   "activation_state",                limit: 32
    t.string   "activation_token",                limit: 64
    t.datetime "activation_token_expires_at"
    t.string   "reset_password_token",            limit: 64
    t.datetime "reset_password_token_expires_at"
    t.datetime "reset_password_email_sent_at"
    t.integer  "failed_logins_count",                         default: 0
    t.datetime "lock_expires_at"
    t.string   "unlock_token",                    limit: 64
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at"
  end

  add_index "user", ["activation_token"], name: "activation_token_UNIQUE", unique: true, using: :btree
  add_index "user", ["email"], name: "unique_user_email", unique: true, using: :btree
  add_index "user", ["provider", "provider_id"], name: "unique_user_provider_provider_id", unique: true, using: :btree

end
