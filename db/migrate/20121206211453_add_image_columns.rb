class AddImageColumns < ActiveRecord::Migration
  def up
    add_attachment :entry, :image
    add_attachment :entry, :banner
    add_attachment :course, :image
    add_attachment :course, :banner
    add_attachment :course, :cover
  end

  def down
    remove_attachment :entry, :image
    remove_attachment :entry, :banner
    remove_attachment :course, :image
    remove_attachment :course, :banner
    remove_attachment :course, :cover
  end
end
