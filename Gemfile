source 'https://rubygems.org'

gem 'rails', '4.0.2'

gem 'rake', '10.1.0'

# Server thin
# gem 'thin'

# Server WEBrick
# gem 'webrick', '1.3.1'

# Bundle edge Rails instead:
# gem 'rails', :git => 'git://github.com/rails/rails.git'

gem 'mysql2'

gem 'sass-rails',   '~> 4.0.1'
gem 'coffee-rails', '~> 4.0.1'
gem 'uglifier', '>= 1.3.2'

# Rails 3 compatibility gems
# gem 'protected_attributes'  # mass assignment

# Bootstrap and JavaScript extensions
gem 'bootstrap-sass', '= 2.3.2.1'
#gem 'bootstrap-sass', '>= 3.0.3.0'
gem 'font-awesome-rails'
gem 'select2-rails'

# jQuery plugin to create "Digital wall" on home page
gem 'masonry-rails'

# A markup language to work with the view files
gem 'haml'

# jQuery for rails
gem 'jquery-rails', '~> 3.0.4'

# Backbone.js Framework
gem 'rails-backbone'

# To use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '>= 3.1.1'

# To use Jbuilder templates for JSON
# gem 'jbuilder'

# Use unicorn as the app server
# gem 'unicorn'

# Deploy with Capistrano
# gem 'capistrano'

# To use debugger
# gem 'debugger'

# Authentication
gem 'sorcery'

# Pagination system
gem 'bootstrap-will_paginate'
#gem 'will_paginate', '~> 3.0'

# Simple form
gem 'simple_form', '>= 3.0.0'
gem 'country_select'
gem 'countries'

# Enumeration type
gem 'simple_enum'

# User roles system
gem 'simple_roles'

# Captcha
gem 'recaptcha', require: 'recaptcha/rails'

# Split up forms on smaller parts
# gem 'wicked'

# Country select
# gem 'country-select'

# Date validator
gem 'date_validator'

# Check if a :belongs_to association actually exists
gem 'validates_existence', '>= 0.4'

# Text and HTML Editor (CKEditor)
gem 'ckeditor', git: 'git://github.com/adamico/ckeditor.git', branch: 'rails-4-compatibility'

# PaperClip to work with images
gem 'paperclip'

# Change the styles blocks in emails to inline styles and helps to keep good appearance.
gem 'roadie'

# It helps to load the page body by Ajax
gem 'turbolinks'
gem 'jquery-turbolinks'

group :development do
  # Add a brakeman tool to generate the report about errors in app.
  # To run this task and generate report file use: rake brakeman:run[doc/reports/brakeman.html]
  gem 'brakeman'

  # Missing joins and counters queries errors detector
  gem 'bullet'

  # Add a mini-profiler to development environment
  gem 'rack-mini-profiler'

  # Model annotations
  gem 'annotate', '>=2.5.0'

  # Turn off assets pipeline log
  gem 'quiet_assets'

  # It shows better error pages
  gem 'better_errors'

  # Additional feature for better errors
  gem 'binding_of_caller', '>=0.7.2'
end

group :test do
  gem 'factory_girl_rails'
  gem 'faker'
  gem 'mocha', require: false

  # RSpec for Unit tests
  gem 'rspec-rails'
  gem 'rspec'

  gem 'cucumber-rails', require: false
  gem 'pickle'

  # database_cleaner is not required, but highly recommended
  gem 'database_cleaner'
end

group :production do
  gem 'rails_12factor'
end

group :stage do
  # Exception notification gem
  gem 'exception_notification'

  # It lets you check email before sending
  gem 'letter_opener'
end

# This version needs to be hardcoded for OpenShift compatibility
gem 'thor', '= 0.18.1'

# This needs to be installed so we can run Rails console on OpenShift directly
gem 'minitest', '~> 4.2'
