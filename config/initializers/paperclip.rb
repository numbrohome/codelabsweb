Paperclip.interpolates :class_name do |attachment, style|
  model_name =attachment.instance.class.to_s.underscore
  list = model_name.split('/')
  list.last.pluralize
end

Paperclip::Attachment.default_options[:path] = ":rails_root/public/files/:class_name/:attachment/:id_partition/:style/:hash.:extension"
Paperclip::Attachment.default_options[:url] = "/files/:class_name/:attachment/:id_partition/:style/:hash.:extension"
Paperclip::Attachment.default_options[:hash_data] = ":class_name/:attachment/:id/:style"
Paperclip::Attachment.default_options[:hash_secret] = "2oz8e6b8x8af8a1bf69a47bn877bl8fz"
Paperclip::Attachment.default_options[:default_url] = "http://placehold.it/120x60&text=No+Image"