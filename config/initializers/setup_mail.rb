ActionMailer::Base.smtp_settings = {
    address:              APP_CONFIG['mailer']['address'],
    port:                 APP_CONFIG['mailer']['port'],
    user_name:            APP_CONFIG['mailer']['user_name'],
    password:             APP_CONFIG['mailer']['password'],
    authentication:       APP_CONFIG['mailer']['authentication'],
    enable_starttls_auto: true
}


# Default host for links generation in view files.
# ActionMailer::Base.default_url_options[:host] = "localhost:3000"

# Interceptor for Development environment.
# Mail.register_interceptor(DevelopmentMailInterceptor) if Rails.env.development?