Project Website:
http://practicalknowledge-marcinkowski.rhcloud.com/

TODO list:
*   Payment system
*   Functional tests with Cucumber
*   Unit tests with RSpec
*   Administration manager for lessons
*   Finish upgrade to Rails 4 by removing attr_accessible etc.

Author:
Marcin Marcinkowski
http://marcinmarcinkowski.com/