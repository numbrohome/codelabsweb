class ValueExistenceValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)

    if options[:model].present? && options[:fields].present?
      valid_on = options[:valid_on] == false ? options[:valid_on] : true
      exists = !!(options[:model].where(options[:fields].join(' = :value OR ').to_s + ' = :value', { value: value }).exists?)

      if exists != valid_on
        record.errors[attribute] << (options[:message] ||  I18n.t('errors.messages.value_existence'))
      end
    end

  end
end