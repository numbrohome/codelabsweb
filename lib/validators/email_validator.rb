class EmailValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/i
      record.errors[attribute] << (options[:message] || I18n.t('errors.messages.not_valid_email'))
    end
  end
end