SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `PracticalKnowledge` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `PracticalKnowledge` ;

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `user` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `provider` VARCHAR(255) NULL DEFAULT NULL,
  `provider_id` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(80) NULL DEFAULT NULL,
  `crypted_password` VARCHAR(255) NOT NULL,
  `salt` VARCHAR(32) NOT NULL,
  `address` VARCHAR(128) NULL,
  `location` VARCHAR(32) NULL,
  `zip_code` VARCHAR(10) NULL,
  `country_id` VARCHAR(2) NULL,
  `website` VARCHAR(100) NULL DEFAULT NULL,
  `phone_number` VARCHAR(20) NULL DEFAULT NULL,
  `role` VARCHAR(30) NOT NULL,
  `first_name` VARCHAR(30) NULL,
  `last_name` VARCHAR(30) NULL,
  `status` TINYINT(2) NOT NULL COMMENT '(enum) baned, disabled, enabled',
  `gender` VARCHAR(1) NOT NULL COMMENT '(enum) male, female',
  `ip` VARCHAR(32) NULL,
  `birth_date` DATE NOT NULL,
  `encrypted_new_email` VARCHAR(255) NULL DEFAULT NULL,
  `identification_token` VARCHAR(64) NULL,
  `remember_me_token` VARCHAR(64) NULL DEFAULT NULL,
  `remember_me_token_expires_at` DATETIME NULL DEFAULT NULL,
  `removal_token` VARCHAR(64) NULL DEFAULT NULL,
  `activation_state` VARCHAR(32) NULL,
  `activation_token` VARCHAR(64) NULL DEFAULT NULL,
  `activation_token_expires_at` DATETIME NULL DEFAULT NULL,
  `reset_password_token` VARCHAR(64) NULL DEFAULT NULL,
  `reset_password_token_expires_at` DATETIME NULL DEFAULT NULL,
  `reset_password_email_sent_at` DATETIME NULL DEFAULT NULL,
  `failed_logins_count` INT NULL DEFAULT 0,
  `lock_expires_at` DATETIME NULL DEFAULT NULL,
  `unlock_token` VARCHAR(64) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `unique_user_email` (`email` ASC),
  UNIQUE INDEX `unique_user_provider_provider_id` (`provider` ASC, `provider_id` ASC),
  UNIQUE INDEX `activation_token_UNIQUE` (`activation_token` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `country` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(32) NOT NULL,
  `tax` DECIMAL(4,2) NOT NULL,
  `code` VARCHAR(2) NOT NULL COMMENT 'Two characters.',
  `currency_code` VARCHAR(3) NOT NULL COMMENT 'eur, usd, pln',
  `currency_symbol` VARCHAR(8) NOT NULL COMMENT 'Label of currency. (zł)',
  `payment_provider` VARCHAR(10) NOT NULL COMMENT '(enum) DotPay',
  `language` VARCHAR(2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `category` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `course`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `course` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` INT UNSIGNED NOT NULL,
  `user_id` INT UNSIGNED NOT NULL,
  `category_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(128) NOT NULL,
  `text` TEXT NULL,
  `short_text` TEXT NULL DEFAULT NULL,
  `price` DECIMAL(8,2) NOT NULL,
  `status` TINYINT NOT NULL COMMENT '(enum) free, regular, pinned',
  `level` TINYINT NOT NULL COMMENT '(enum) beginner, intermediate, advanced',
  `subject` TINYINT NOT NULL COMMENT '(enum), audio, video, text',
  `duration` INT NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `index_course_country` (`country_id` ASC),
  INDEX `index_course_user` (`user_id` ASC),
  INDEX `index_course_category` (`category_id` ASC),
  CONSTRAINT `fk_course_country`
    FOREIGN KEY (`country_id`)
    REFERENCES `country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_course_category`
    FOREIGN KEY (`category_id`)
    REFERENCES `category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cart`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cart` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NULL DEFAULT NULL,
  `net_price` DECIMAL(9,2) NOT NULL,
  `gross_price` DECIMAL(9,2) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cart_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_cart_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `order` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NULL,
  `cart_id` INT UNSIGNED NOT NULL,
  `status` TINYINT NOT NULL COMMENT 'new 		- When user create the order.\npaid 		- When our system get information from payment provider that order is paid.\nreleased 	- When our system gives an access to content.\ndelieverd 	- When user download a content for first time.\nsent 		- When we send  /* comment truncated */ /*the stuff by mail or email.
received 	- When we send the stuff and customer received them.
lost 		- When stuff are lost.
archived 	- When download counter hits the limit or time for usage is expired.*/',
  `download_counter` INT UNSIGNED NULL DEFAULT NULL COMMENT 'For download count based access limitation.',
  `expired_at` DATETIME NULL DEFAULT NULL COMMENT 'For time based access limitation.',
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `index_purchase_user` (`user_id` ASC),
  INDEX `fk_order_cart1_idx` (`cart_id` ASC),
  CONSTRAINT `fk_purchase_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_order_cart1`
    FOREIGN KEY (`cart_id`)
    REFERENCES `cart` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `lesson`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lesson` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `course_id` INT UNSIGNED NOT NULL COMMENT 'A foreign key of coure.',
  `user_id` INT UNSIGNED NOT NULL COMMENT 'Author of the lesson',
  `title` VARCHAR(128) NOT NULL,
  `text` TEXT NULL DEFAULT NULL,
  `short_text` TEXT NULL DEFAULT NULL COMMENT '(enum) text, video, pdf ',
  `price` DECIMAL(8,2) NULL DEFAULT NULL,
  `time` INT NULL DEFAULT NULL COMMENT 'How long is video.',
  `content_url` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Link to video lesson.',
  `example_url` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Link to file with code examples.',
  `type_of_content` TINYINT UNSIGNED NOT NULL COMMENT 'Type of content: video, text, other, mixed, audio',
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `index_lesson_course` (`course_id` ASC),
  INDEX `index_lesson_user` (`user_id` ASC),
  CONSTRAINT `fk_lesson_course`
    FOREIGN KEY (`course_id`)
    REFERENCES `course` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_lesson_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `entry`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `entry` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `country_id` INT UNSIGNED NOT NULL,
  `title` VARCHAR(128) NOT NULL,
  `status` TINYINT NOT NULL COMMENT '(enum) disabled, enabled, pinned',
  `main_display_mode` TINYINT NOT NULL COMMENT '(enum) hidden, small, medium, big',
  `text` TEXT NOT NULL,
  `short_text` TEXT NULL,
  `hits` INT UNSIGNED NULL DEFAULT 0,
  `code` TEXT NULL COMMENT 'It\'s a code display function for home page to show something different instead of image.',
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `index_entry_user` (`user_id` ASC),
  INDEX `index_entry_country` (`country_id` ASC),
  CONSTRAINT `fk_entry_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_entry_country`
    FOREIGN KEY (`country_id`)
    REFERENCES `country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cart_item`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cart_item` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cart_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(128) NOT NULL,
  `net_price` DECIMAL(9,2) NOT NULL,
  `gross_price` DECIMAL(9,2) NOT NULL,
  `quantity` INT UNSIGNED NULL DEFAULT 1,
  `link` VARCHAR(255) NULL DEFAULT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cart_item_cart1_idx` (`cart_id` ASC),
  CONSTRAINT `fk_cart_item_cart1`
    FOREIGN KEY (`cart_id`)
    REFERENCES `cart` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `payment` (
  `id` INT NOT NULL,
  `order_id` INT UNSIGNED NOT NULL,
  `external_id` VARCHAR(128) NULL DEFAULT NULL COMMENT 'Depends of payment provider.',
  `payment_provider` VARCHAR(10) NOT NULL,
  `status` TINYINT NULL COMMENT 'Depends of payment provider.',
  `amount` DECIMAL(9,2) NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_order1_idx` (`order_id` ASC),
  CONSTRAINT `fk_payment_order1`
    FOREIGN KEY (`order_id`)
    REFERENCES `order` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
