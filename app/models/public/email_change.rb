class Public::EmailChange
  include ActiveModel::Model

  attr_accessor :user, :password, :new_email

  validates :user,                        presence:         true
  validates :password,                    presence:         true,
                                          authentication:   { user: :user }
  validates :new_email,                   presence:         true,
                                          email:            true,
                                          value_existence:  { valid_on: false, model: BaseModel::User, fields: [ :email ], message: I18n.t('errors.messages.email_already_exists')}

  def deliver_email_change_instructions!
    self.user.new_email = self.new_email
    self.valid? && self.user.save && UserMailer.change_email_email(self.user).deliver
  end
end