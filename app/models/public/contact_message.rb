class Public::ContactMessage
  include ActiveModel::Model

  attr_accessor :captcha, :email, :name, :subject, :body

  validates :email,                       presence:     true,
                                          email:        true,
                                          length:       { maximum: 200 }
  validates :name,                        presence:     true,
                                          length:       3..100
  validates :subject,                     presence:     true,
                                          length:       3..100
  validates :body,                        presence:     true,
                                          length:       10..500000

  def send_contact_message
    valid? && ContactMessageMailer.contact_message_email(self).deliver
  end
end