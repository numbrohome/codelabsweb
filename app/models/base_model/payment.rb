class BaseModel::Payment < ActiveRecord::Base
  self.table_name = 'payment'

  belongs_to :order

  attr_accessor :id, :order, :external_id, :payment_provider, :status, :amount, :created_at, :updated_at

end