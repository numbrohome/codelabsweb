class BaseModel::Lesson < ActiveRecord::Base
  self.table_name = 'lesson'

  belongs_to :user
  belongs_to :course

  scope :video,           -> { where(type_of_content: self.content_types.video) }
  scope :audio,           -> { where(type_of_content: self.content_types.audio) }
  scope :text,            -> { where(type_of_content: self.content_types.text) }
  scope :mixed,           -> { where(type_of_content: self.content_types.mixed) }
  scope :free,            -> { where(price: nil) }
  scope :course_lesson,   ->(course_id, lesson_id) { where('lesson.course_id = ? AND lesson.id = ?', course_id, lesson_id).first if course_id.present? && lesson_id.present? }

  #TODO: Use different names for type's name and column name.
  as_enum :content_type,  { other: 0, mixed: 1, text: 2, audio: 3, video: 4 }, column: 'type_of_content'

  validates :text,              length:       100..100000,
                                allow_blank:  true
  validates :short_text,        length:       50..10000,
                                allow_blank:  true
  validates :title,             presence:     true,
                                length:       1..128
  validates :time,              allow_blank:  true,
                                numericality: { only_integer: true }
  validates :price,             allow_blank:  true,
                                format:       { with: /\A\d+(\.\d{1,2})?\z/ },
                                numericality: { greater_than_or_equal_to: 0.01, less_than: 100000.00 }
  validates :content_url,       length:       10..256,
                                allow_blank:  true
  validates :example_url,       length:       10..256,
                                allow_blank:  true
  validates :type_of_content,   presence:     true,
                                inclusion:    { in: self.content_types.values }
  validates :user,              presence:     true,
                                existence:    true
  validates :course,            presence:     true,
                                existence:    true

  def full_price
    self.price.to_s + ' ' + self.course.country.currency_code
  end
end
