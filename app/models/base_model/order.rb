class BaseModel::Order < ActiveRecord::Base
  self.table_name = 'order'

  belongs_to :user
  has_one :cart
  has_many :payments

  attr_accessor :id, :user, :cart, :status, :download_counter, :expired_at, :created_at, :updated_at

end