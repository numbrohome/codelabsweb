class BaseModel::Course < ActiveRecord::Base
  self.table_name = 'course'
  self.per_page = 10

  has_many :lessons
  belongs_to :user
  belongs_to :country
  belongs_to :category

  as_enum :status_type,  { error: 0, disabled: 1, free: 2, regular: 3, pinned: 4 }, column: 'status'
  as_enum :level_type,   { error: 0, beginner: 1, intermediate: 2, advanced: 3 }, column: 'level'
  as_enum :subject_type, { error: 0, text: 1, audio: 2, video: 3 }, column: 'subject'

  scope :disabled,          -> { where(status: self.status_types.disabled) }
  scope :free,              -> { where(status: self.status_types.free) }
  scope :regular,           -> { where(status: self.status_types.regular) }
  scope :pinned,            -> { where(status: self.status_types.pinned) }
  scope :public_accessible, -> { where(status: [self.status_types.free, self.status_types.regular, self.status_types.pinned]) }
  scope :last_week,         -> { where('created_at < ?', Time.zone.now) }
  scope :language,          ->(country_code) { where( 'country.code = ?', country_code ).joins(:country) }
  scope :level,             ->(level) { where( 'course.level = ?', level ) if level.present? }
  scope :category,          ->(category) { where( 'course.category_id = ?', category ) if category.present? }
  scope :editor,            ->(editor) { where( 'course.user_id = ?', editor ) if editor.present? }
  scope :subject,           ->(subject) { where( 'course.subject = ?', subject ) if subject.present? }
  scope :search,            ->(keywords) { where('course.title LIKE :keywords OR course.short_text LIKE :keywords', keywords: "%#{keywords}%") }

  has_attached_file :cover, styles: {
      large:    [ '430x600#', :jpg, ],
      medium:   [ '180x248#', :jpg, ],
      small:    [ '100x140#', :jpg, ],
      thumb:    [ '200x100>', :jpg, ]
  },
  convert_options: {
      large:    '-resize 430x600^ -gravity center -crop 430x600+0+0 -quality 80',
      medium:   '-quality 80',
      small:    '-quality 80'
  }

  has_attached_file :image, styles: {
      large:    [ '1200x300#', :jpg, ],
      medium:   [ '580x296#', :jpg, ],
      small:    [ '285x194#', :jpg, ],
      thumb:    [ '200x120>', :jpg, ]
  },
  convert_options: {
      large:    '-resize 1200x300^ -gravity center -crop 1200x300+0+0 -quality 80',
      medium:   '-quality 80',
      small:    '-quality 80'
  }

  has_attached_file :banner, styles: {
      regular:  [ '1200x150#', :jpg ],
      thumb:    [ '200x120>', :jpg ]
  },
  convert_options: {
      regular:  '-resize 1200x150^ -gravity center -crop 1200x150+0+0 -quality 80'
  }

  validates :title,             presence:     true,
                                length:       5..128
  validates :text,              length:       100..100000,
                                allow_blank:  true
  validates :short_text,        length:       50..10000,
                                allow_blank:  true
  validates :price,             format:       { with: /\d+(\.\d{1,2})?/ },
                                numericality: { greater_than_or_equal_to: 0.00, less_than: 100000 }
  validates :status,            presence:     true,
                                inclusion:    { in: self.status_types.values }
  validates :level,             presence:     true,
                                inclusion:    { in: self.level_types.values }
  validates :subject,           presence:     true,
                                inclusion:    { in: self.subject_types.values }
  validates :duration,          presence:     true,
                                numericality: { only_integer: true }
  validates :country,           presence:     true,
                                existence:    true
  validates :user,              presence:     true,
                                existence:    true
  validates :category,          presence:     true,
                                existence:    true

  validates :cover,             attachment_size: 0..5.megabytes,
                                attachment_content_type: { content_type: ['image/jpeg', 'image/png'], message: 'form.error.file_type' }
  validates :image,             attachment_size: 0..5.megabytes,
                                attachment_content_type: { content_type: ['image/jpeg', 'image/png'], message: 'form.error.file_type' }
  validates :banner,            attachment_size: 0..5.megabytes,
                                attachment_content_type: { content_type: ['image/jpeg', 'image/png'], message: 'form.error.file_type' }

  after_initialize :init

  def full_price
    self.price.to_s + ' ' + self.country.currency_code
  end

  # Callback "before_validate"
  # Set the default attributes.
  def init
    self.status ||= BaseModel::Course.status_types.disabled
    self.price ||= 0
  end
end
