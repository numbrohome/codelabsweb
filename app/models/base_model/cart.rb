class BaseModel::Cart < ActiveRecord::Base
  self.table_name = 'cart'

  belongs_to :user
  belongs_to :order
  has_many :cart_items

  attr_accessor :id, :user, :net_price, :gross_price, :created_at, :updated_at

end