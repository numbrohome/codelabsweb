class BaseModel::CartItem < ActiveRecord::Base
  self.table_name = 'cart_item'

  belongs_to :cart

  attr_accessor :id, :cart, :net_price, :gross_price, :name, :quantity, :link, :created_at, :updated_at

end