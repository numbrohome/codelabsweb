# == Schema Information
#
# Table name: purchase
#
#  id               :integer          not null, primary key
#  user_id          :integer          not null
#  content_id       :integer          not null
#  type             :integer          not null
#  status           :integer          not null
#  expire           :datetime
#  value            :decimal(8, 2)    not null
#  download_counter :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime
#

class BaseModel::Purchase < ActiveRecord::Base
  self.table_name = 'purchase'

  attr_accessor :content_id, :expire, :status, :type, :value

  belongs_to :user

  as_enum :status,  :error => 0, :initial => 1, :expected => 2, :unpaid => 3, :unlocked => 4, :paid => 5, :archival => 6
  as_enum :type,    :lesson => 0, :course => 1

  validates :content_id,        :presence     => true,
                                :numericality => { :only_integer => true }
  validates :download_counter,  :presence     => true,
                                :numericality => { :only_integer => true, :greater_than_or_equal_to => 0 }
  validates :expire,            :allow_blank  => true,
                                :date         => true
  validates :value,             :presence     => true,
                                :format       => { :with => /\A\d+(\.\d{1,2})?\z/ },
                                :numericality => { :greater_than_or_equal_to => 0.00, :less_than => 100000 }
  validates :status,            :inclusion    => { :in => BaseModel::Purchase.statuses.keys }
  validates :type,              :inclusion    => { :in => BaseModel::Purchase.types.keys }


end
