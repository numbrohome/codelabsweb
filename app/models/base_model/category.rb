class BaseModel::Category < ActiveRecord::Base
  self.table_name = 'category'

  has_many :courses

  validates :name,              presence:     true,
                                length:       1..100
end
