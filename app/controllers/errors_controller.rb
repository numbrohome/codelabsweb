class ErrorsController < ApplicationController
  skip_before_filter :require_login, only: [:show]
  layout 'application'

  attr_accessor :error_codes

  before_action :set_error_codes

  def set_error_codes
    @error_codes = {
      '404' => { error_code: '404', message: 'Not Found' },
      '401' => { error_code: '401', message: 'Unauthorized' },
    }
  end

  def show
    error_code = @error_codes.has_key?(params[:id]) ? params[:id] : '404'

    respond_to do |format|
      format.html { render error_code, status: error_code }
      format.json { render json: @error_codes.fetch(error_code), status: error_code }
    end
  end
end