class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_locale, :require_login, :set_mailer_host

  rescue_from BaseModel::User::AuthenticationError, with: proc { redirect_to(login_url, alert: t('notice.alert.have_to_be_login')) }
  rescue_from BaseModel::User::AuthorizationError, with: proc { redirect_to error_path(401) }
  rescue_from ActionController::RoutingError, with: proc { redirect_to error_path(404) }

  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options(options={})
    logger.debug "default_url_options is passed options: #{ options.inspect }\n"
    { locale: I18n.locale }
  end

  private

  def not_authenticated
    fail BaseModel::User::AuthenticationError, 'Not Authenticated'
  end

  def not_authorized
    fail BaseModel::User::AuthorizationError, 'Not Authorized'
  end

  def not_found
    fail ActionController::RoutingError, 'Not Found'
  end

  def redirect_logged_in
    redirect_to(controller: 'public/dashboards', action: 'show')
  end

  def increase_hits(model, expires = 86400)
    unique_name = Digest::MD5.hexdigest(model.class.name+model.id.to_s+(current_user ? current_user.id.to_s : ''))

    if cookies[unique_name].blank?
      model.hits += 1
      model.save
      cookies[unique_name] = { value: true, expires: Time.now + expires}
    end
  end

  def login(*credentials)
    @current_user = nil
    user = user_class.authenticate(*credentials)

    if user && user.able_to_logged_in?

      old_session = session.dup.to_hash
      reset_session # protect from session fixation attacks
      old_session.each_pair do |k,v|
        session[k.to_sym] = v
      end
      form_authenticity_token

      auto_login(user)
      after_login!(user, credentials)
      current_user
    else
      after_failed_login!(credentials)
      nil
    end
  end
end
