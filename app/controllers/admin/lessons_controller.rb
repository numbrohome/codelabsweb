class Admin::LessonsController < Admin::AdminController
  # GET /admin/lessons
  # GET /admin/lessons.json
  def index
    @course = BaseModel::Course.find_by_id(params[:course_id])
    @lessons = @course.lessons

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @lessons }
    end
  end

  # GET /admin/lessons/1
  # GET /admin/lessons/1.json
  def show
    @course = BaseModel::Course.find_by_id(params[:course_id]) || not_found
    @lesson = BaseModel::Lesson.course_lesson(@course.id, params[:id]) || not_found

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @lesson }
    end
  end

  # GET /admin/lessons/new
  # GET /admin/lessons/new.json
  def new
    @course = BaseModel::Course.find_by_id(params[:course_id]) || not_found
    @lesson = BaseModel::Lesson.new
    @lesson.course = @course

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lesson }
    end
  end

  # GET /admin/lessons/1/edit
  def edit
    @course = BaseModel::Course.find_by_id(params[:course_id]) || not_found
    @lesson = BaseModel::Lesson.course_lesson(@course.id, params[:id]) || not_found
  end

  # POST /admin/lessons
  # POST /admin/lessons.json
  def create
    @course = BaseModel::Course.find_by_id(params[:course_id])
    @lesson = BaseModel::Lesson.new(lesson_params)

    respond_to do |format|
      if @lesson.save
        format.html { redirect_to admin_course_lesson_path(@course.id, @lesson.id), notice: 'Lesson was successfully created.' }
        format.json { render json: @lesson, status: :created, location: @lesson }
      else
        format.html { render action: 'new' }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/lessons/1
  # PUT /admin/lessons/1.json
  def update
    @course = BaseModel::Course.find_by_id(params[:course_id]) || not_found
    @lesson = BaseModel::Lesson.course_lesson(@course.id, params[:id]) || not_found

    respond_to do |format|
      if @lesson.update_attributes(lesson_params)
        format.html { redirect_to edit_admin_course_lesson_path(@course.id, @lesson.id), notice: 'Lesson was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lesson.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/lessons/1
  # DELETE /admin/lessons/1.json
  def destroy
    @course = BaseModel::Course.find_by_id(params[:course_id]) || not_found
    @lesson = BaseModel::Lesson.course_lesson(@course.id, params[:id]) || not_found
    @lesson.destroy

    respond_to do |format|
      format.html { redirect_to admin_course_lessons_path }
      format.json { head :no_content }
    end
  end

  private

  def lesson_params
    params.require(:base_model_lesson).permit(:price, :text, :short_text, :time, :title, :type_of_content, :user_id, :course_id, :content_url, :example_url)
  end
end
