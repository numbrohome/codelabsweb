class Admin::CountriesController < Admin::AdminController
  helper_method :sort_column, :sort_direction

  # GET /admin/countries
  # GET /admin/countries.json
  def index
    @countries = BaseModel::Country.paginate(:page => params[:page]).order(sort_column + " " + sort_direction)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @countries }
    end
  end

  # GET /admin/countries/1
  # GET /admin/countries/1.json
  def show
    @country = BaseModel::Country.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @country }
    end
  end

  # GET /admin/countries/new
  # GET /admin/countries/new.json
  def new
    @country = BaseModel::Country.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @country }
    end
  end

  # GET /admin/countries/1/edit
  def edit
    @country = BaseModel::Country.find(params[:id])
  end

  # POST /admin/countries
  # POST /admin/countries.json
  def create
    @country = BaseModel::Country.new(params[:admin_country])

    respond_to do |format|
      if @country.save
        format.html { redirect_to :action => "index", notice: 'Country was successfully created.' }
        format.json { render json: @country, status: :created, location: @country }
      else
        format.html { render action: "new" }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/countries/1
  # PUT /admin/countries/1.json
  def update
    @country = BaseModel::Country.find(params[:id])

    respond_to do |format|
      if @country.update_attributes(params[:admin_country])
        format.html { redirect_to :action => "index", notice: 'Country was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @country.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/countries/1
  # DELETE /admin/countries/1.json
  def destroy
    @country = BaseModel::Country.find(params[:id])
    @country.destroy

    respond_to do |format|
      format.html { redirect_to admin_countries_url }
      format.json { head :no_content }
    end
  end

  private

  def country_params
    params.require(:base_model_country).permit(:name, :code, :currency_code, :currency_symbol, :payment_provider, :tax, :language)
  end

  def sort_column
    BaseModel::Country.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
