class Admin::CategoriesController < Admin::AdminController
  helper_method :sort_column, :sort_direction

  # GET /admin/categories
  # GET /admin/categories.json
  def index
    @categories = BaseModel::Category.paginate(:page => params[:page]).order(sort_column + " " + sort_direction)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @categories }
    end
  end

  # GET /admin/categories/1
  # GET /admin/categories/1.json
  def show
    @category = BaseModel::Category.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @category }
    end
  end

  # GET /admin/categories/new
  # GET /admin/categories/new.json
  def new
    @category = BaseModel::Category.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @category }
    end
  end

  # GET /admin/categories/1/edit
  def edit
    @category = BaseModel::Category.find(params[:id])
  end

  # POST /admin/categories
  # POST /admin/categories.json
  def create
    @category = BaseModel::Category.new(params[:admin_category])

    respond_to do |format|
      if @category.save
        format.html { redirect_to :action => "index", notice: 'Category was successfully created.' }
        format.json { render json: @category, status: :created, location: @category }
      else
        format.html { render action: "new" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/categories/1
  # PUT /admin/categories/1.json
  def update
    @category = BaseModel::Category.find(params[:id])

    respond_to do |format|
      if @category.update_attributes(params[:admin_category])
        format.html { redirect_to :action => "index", notice: 'Category was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/categories/1
  # DELETE /admin/categories/1.json
  def destroy
    @category = BaseModel::Category.find(params[:id])
    @category.destroy

    respond_to do |format|
      format.html { redirect_to admin_categories_url }
      format.json { head :no_content }
    end
  end

  private

  def category_params
    params.require(:base_model_category).permit(:name)
  end

  def sort_column
    BaseModel::Category.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
