class Admin::CoursesController < Admin::AdminController
  # GET /admin/courses
  # GET /admin/courses.json
  def index
    @courses = BaseModel::Course.paginate(:page => params[:page]).order('id DESC').includes(:country)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @courses }
    end
  end

  # GET /admin/courses/1
  # GET /admin/courses/1.json
  def show
    @course = BaseModel::Course.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @course }
    end
  end

  # GET /admin/courses/new
  # GET /admin/courses/new.json
  def new
    @course = BaseModel::Course.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @course }
    end
  end

  # GET /admin/courses/1/edit
  def edit
    @course = BaseModel::Course.find(params[:id])
  end

  # POST /admin/courses
  # POST /admin/courses.json
  def create
    @course = BaseModel::Course.new(course_params)

    respond_to do |format|
      if @course.save
        format.html { redirect_to @course, notice: 'Course was successfully created.' }
        format.json { render json: @course, status: :created, location: @course }
      else
        format.html { render action: 'new' }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/courses/1
  # PUT /admin/courses/1.json
  def update
    @course = BaseModel::Course.find(params[:id])

    respond_to do |format|
      if @course.update_attributes(course_params)
        format.html { redirect_to edit_admin_course_path(@course.id), notice: 'Course was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/courses/1
  # DELETE /admin/courses/1.json
  def destroy
    @course = BaseModel::Course.find(params[:id])
    @course.destroy

    respond_to do |format|
      format.html { redirect_to courses_url }
      format.json { head :no_content }
    end
  end

  private

  def course_params
    params.require(:base_model_course).permit(:user_id, :country_id, :category_id, :title, :text, :short_text, :price, :status, :level, :subject, :duration, :image, :banner, :cover)
  end
end
