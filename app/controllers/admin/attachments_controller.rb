class Admin::AttachmentsController < Admin::AdminController
  include ApplicationHelper

  def delete
  end

  def destroy
    begin
      model = params[:model].constantize

      if model.respond_to?('find_by_id')
        obj = model.find_by_id(params[:id])

        if obj.respond_to?(params[:attachment])
           obj.send(params[:attachment]).destroy
           obj.send(params[:attachment]).clear
           obj.save
        end
      end
    rescue NameError => e
      raise ActionController::RoutingError.new(e.message)
    end

    if request.xhr?
      render :text => 'OK'
    else
      redirect_to params.include?(:return_url) ? decrypt(params[:return_url]) : root_path
    end
  end
end
