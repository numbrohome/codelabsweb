class Admin::EntriesController < Admin::AdminController
  helper_method :sort_column, :sort_direction

  # GET /admin/entries
  # GET /admin/entries.json
  def index
    @entries = BaseModel::Entry.paginate(page: params[:page]).order(sort_column + " " + sort_direction).includes(:country)

    respond_to do |format|
      format.html # index.html.haml
      format.json { render json: @entries }
    end
  end

  # GET /admin/entries/1
  # GET /admin/entries/1.json
  def show
    @entry = BaseModel::Entry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @entry }
    end
  end

  # GET /admin/entries/new
  # GET /admin/entries/new.json
  def new
    @entry = BaseModel::Entry.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @entry }
    end
  end

  # GET /admin/entries/1/edit
  def edit
    @entry = BaseModel::Entry.find(params[:id])
  end

  # POST /admin/entries
  # POST /admin/entries.json
  def create
    @entry = BaseModel::Entry.new(entry_params)

    respond_to do |format|
      if @entry.save
        format.html { redirect_to admin_entry_path(@entry), notice: 'Entry was successfully created.' }
        format.json { render json: @entry, status: :created, location: @entry }
      else
        format.html { render action: "new" }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /admin/entries/1
  # PUT /admin/entries/1.json
  def update
    @entry = BaseModel::Entry.find(params[:id])

    respond_to do |format|
      if @entry.update_attributes(entry_params)
        format.html { redirect_to edit_admin_entry_path(@entry.id), notice: 'Entry was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/entries/1
  # DELETE /admin/entries/1.json
  def destroy
    @entry = BaseModel::Entry.find(params[:id])
    @entry.destroy

    respond_to do |format|
      format.html { redirect_to admin_entries_url }
      format.json { head :no_content }
    end
  end

  private

  def entry_params
    params.require(:base_model_entry).permit(:user_id, :country_id, :status, :main_display_mode, :text, :short_text, :code, :title, :image, :banner)
  end

  def sort_column
    BaseModel::Entry.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
  end
end
