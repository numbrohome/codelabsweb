class Public::EmailChangesController < Public::PublicController

  skip_before_filter :require_login, only: [ :update ]

  layout 'dashboard'

  def new
    @email_change = Public::EmailChange.new
    render 'new'
  end

  def create
    @email_change = Public::EmailChange.new(email_change_params.merge(user: current_user))

    respond_to do |format|
      if @email_change.deliver_email_change_instructions!
        format.html do
          flash[:success] = t 'notice.success.email_change_instructions_sent'
          redirect_to controller: 'public/dashboards', action: 'show'
        end
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @email_change.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @user = BaseModel::User.load_from_encrypted_new_email(params[:id] || params[:token]) || not_authorized

    respond_to do |format|
      if @user.reset_email(@user.new_email)
        format.html do
          flash[:success] = t 'notice.success.email_changed'
          if logged_in?
            redirect_to controller: 'public/dashboards', action: 'show'
          else
            redirect_to controller: 'public/sessions', action: 'new'
          end
        end
        format.json { head :no_content }
      else
        format.html do
          flash[:error] = t 'notice.error.email_already_exists'
          if logged_in?
            redirect_to controller: 'public/dashboards', action: 'show'
          else
            redirect_to controller: 'public/sessions', action: 'new'
          end
        end
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def email_change_params
    params.require(:public_email_change).permit(:password, :new_email)
  end

end