class Public::SearchesController < Public::PublicController
  skip_before_filter :require_login

  def show
    @search = Public::Search.new(keywords: params[:keywords])
    @courses = []
    @entries = []

    respond_to do |format|
      if @search.valid?
        is_admin = logged_in? && current_user.is_admin?
        @courses = @search.find_courses(params[:courses_page], is_admin)
        @entries = @search.find_entries(params[:entries_page], is_admin)

        format.html
        format.json { render json: { courses: @courses, entries: @entries } }
      elsif
        format.html
        format.json { render json: @search.errors, status: :unprocessable_entity }
      end
    end
  end
end