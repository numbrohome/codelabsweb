class Public::UsersController < Public::PublicController
  skip_before_filter :require_login, only: [ :index, :new, :create, :activate, :created, :destroy, :remove, :removed ]

  layout :resolve_layout


  def new
    return redirect_logged_in if logged_in?

    @user = BaseModel::User.new
    render 'new'
  end


  def create
    return redirect_logged_in if logged_in?

    @user = BaseModel::User.new(user_create_params)
    @user.ip = request.env['HTTP_X_FORWARDED_FOR'] || request.remote_addr

    respond_to do |format|
      if verify_recaptcha(model: @user, attribute: :captcha, message: t('form.error.captcha')) && @user.save
        format.html { redirect_to action: 'created', token: @user.identification_token }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def created
    return redirect_logged_in if logged_in?

    @user = BaseModel::User.load_from_identification_token(params[:id] || params[:token]) || not_authorized
    render 'created'
  end


  def activate
    return redirect_logged_in if logged_in?

    @user = BaseModel::User.load_from_activation_token(params[:id] || params[:token]) || not_authorized

    respond_to do |format|
      if @user.activate!
        format.html do
          auto_login(@user)
          redirect_to action: 'activated', token: @user.identification_token
        end
        format.json { head :no_content }
      else
        format.html { not_authorized }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def activated
    @user = BaseModel::User.load_from_identification_token(params[:id] || params[:token]) || not_authorized
    render 'activated'
  end


  def edit
    @user = current_user
    render 'edit'
  end


  def update
    @user = current_user

    respond_to do |format|
      if @user.update_settings!(user_update_params)
        format.html do
          flash[:success] = t 'notice.success.settings_updated'
          redirect_to action: 'edit'
        end
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def delete
    @user = current_user
    render 'delete'
  end


  def activate_removal
    @user = current_user

    respond_to do |format|
      if BaseModel::User.authenticate(@user.email, params[:public_user][:password]) && @user.deliver_removal_instructions
        format.html do
          flash[:success] = t 'notice.success.activate_account_removal'
          redirect_to controller: 'public/dashboards', action: 'show'
        end
        format.json { head :no_content }
      else
        @user.errors.add(:password, t('form.error.wrong_password'))
        format.html { render action: 'delete' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @user = BaseModel::User.load_from_removal_token(params[:id] || params[:token]) || not_authorized

    respond_to do |format|
      if @user.remove
        logout
        format.html { redirect_to action: 'removed', token: @user.identification_token }
        format.json { head :no_content }
      else
        format.html { not_authorized }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end


  def removed
    @user = BaseModel::User.load_from_identification_token(params[:id] || params[:token]) || not_authorized
    render 'removed'
  end


  private


  def resolve_layout
    case action_name
    when 'edit', 'delete', 'update', 'activate_removal'
      'dashboard'
    else
      'application'
    end
  end


  def user_create_params
    params.require(:public_user).permit(:email, :password, :password_confirmation, :birth_date, :gender, :captcha, :terms_of_service)
  end


  def user_update_params
    params.require(:public_user).permit(:first_name, :last_name, :website, :phone_number, :country_id, :address, :location, :zip_code)
  end
end