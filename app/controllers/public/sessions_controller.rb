class Public::SessionsController < Public::PublicController
  skip_before_filter :require_login, except: [:destroy]

  def new
    return redirect_logged_in if logged_in?
    @session = Public::Session.new
    render 'new'
  end

  def create
    return redirect_logged_in if logged_in?

    @session = Public::Session.new(session_params)

    respond_to do |format|
      if @session.valid? && @user = login(session_params[:email], session_params[:password], session_params[:remember_me])
        format.html { redirect_back_or_to(dashboard_path, success: t('notice.success.login_successful')) }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html do
          @session.errors.add :password, t('notice.error.login_failed');
          render action: 'new'
        end
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    logout
    redirect_to(root_url, flash: { info: t('notice.info.logged_out') } )
  end

  private

  def session_params
    params.require(:public_session).permit(:email, :password, :remember_me)
  end
end
