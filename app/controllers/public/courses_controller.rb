class Public::CoursesController < Public::PublicController
  skip_before_filter :require_login, :only => [:index, :show]

  def index
    @courses = BaseModel::Course
      .language(I18n.locale)
      .level(params[:level])
      .category(params[:category])
      .editor(params[:editor])
      .subject(params[:subject])
      .paginate(page: params[:page])
      .order('course.status DESC, course.id DESC')
      .includes(:country, :user, :category)
    @categories = BaseModel::Category.all
    @editors = BaseModel::User.editors

    respond_to do |format|
      format.html
      format.json { render json: @courses }
    end
  end

  def show
    @course = BaseModel::Course.public_accessible.find_by_id(params[:id]) || not_found

    respond_to do |format|
      format.html
      format.json { render json: @course }
    end
  end
end
