class Public::EntriesController < Public::PublicController
  skip_before_filter :require_login

  def show
    @entry = BaseModel::Entry.public_accessible.find_by_id(params[:id]) || not_found
    increase_hits(@entry)

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @entry }
    end
  end
end
