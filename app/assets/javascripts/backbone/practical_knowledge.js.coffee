#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.PracticalKnowledge =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}