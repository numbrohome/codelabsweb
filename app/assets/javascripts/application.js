// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery.turbolinks
//= require turbolinks
//= require jquery_ujs
//= require underscore
//= require backbone
//= require backbone_rails_sync
//= require backbone_datalink
//= require backbone/practical_knowledge
//= require_tree .
//= require bootstrap
//= require select2
//= require ckeditor/init
//= require masonry/jquery.masonry

$(document).ready(function() {

    // Select2 feature for drop down lists.
    $(".select2").select2();

    // FitText Feature for responsive text size.
    $(".title-fit-text").fitText(3.6, { minFontSize: '25.0pc', maxFontSize: '30.0pc' });

    // Home page and flexible elements with Masonry plugin.
    var digitalWall = $('.digital-wall');
    var lastDigitalWallWidth = digitalWall.width();

    var searchResult = $('.search-result');
    var lastSearchResultWidth = searchResult.width();

    // Force load for masonry plugin when page is created first time.
    resizeDigitalWallElements(true);

    // Force
    resizeCourseListElements();

    // Resize search elements for first time when page is loaded
    resizeSearchElements(true);

    // Window resize event.
    $(window).resize(function() {
        resizeDigitalWallElements(false); // Reload masonry plugin.
        resizeCourseListElements();
        resizeSearchElements(false);
    });

    function resizeCourseListElements()
    {
        var courseListImages = $('.course-list-image');

        if ($('#courses-list').width()<500) {
            courseListImages.fadeOut(200);
        } else {
            courseListImages.fadeIn(200);
        }
    }

    function resizeSearchElements(force)
    {
        var width = searchResult.width();
        var spaceBetweenElements = 10;

        if (lastSearchResultWidth == width && force == false)
            return false;

        if (width<=580) {
            $('.search-result-item').width('100%');
            columnWidth = width;
        } else if (width<940 && width>580) {
            $('.search-result-item').width((width-spaceBetweenElements)/2);
            columnWidth = (width-spaceBetweenElements)/2;
        } else {
            $('.search-result-item').width((width-2*spaceBetweenElements)/3);
            columnWidth = (width-2*spaceBetweenElements)/3;
        }

        searchResult.masonry({
            itemSelector: '.search-result-item',
            columnWidth: columnWidth,
            gutterWidth: spaceBetweenElements
        });

        lastSearchResultWidth = width;
        return true;
    }

    function resizeDigitalWallElements(force)
    {
        var spaceBetweenElements = 10;
        var width = digitalWall.width();

        if (lastDigitalWallWidth == width && force == false)
            return false;

        if (width<=580) {
            $('.display-mode-medium').width('100%');
            $('.display-mode-small').width('100%');
            // Every box has medium size of image
            $('.digital-box-image').each(function(){
                $(this).css("background-image", "url("+$(this).attr('data-image-src-medium')+")");
            });
        } else if (width<940 && width>580) {
            $('.display-mode-medium').width((width-spaceBetweenElements)/2);
            $('.display-mode-small').width($('.display-mode-medium').width());

            $('.display-mode-image-small').each(function(){
                $(this).css("background-image", "url("+$(this).attr('data-image-src-medium')+")");
            });
            $('.display-mode-image-large').each(function(){
                $(this).css("background-image", "url("+$(this).attr('data-image-src-large')+")");
            });
        } else {
            $('.display-mode-medium').width((width-spaceBetweenElements)/2);
            $('.display-mode-small').width((width-3*spaceBetweenElements)/4);

            $('.display-mode-image-small').each(function(){
                $(this).css("background-image", "url("+$(this).attr('data-image-src-small')+")");
            });
            $('.display-mode-image-medium').each(function(){
                $(this).css("background-image", "url("+$(this).attr('data-image-src-medium')+")");
            });
            $('.display-mode-image-large').each(function(){
                $(this).css("background-image", "url("+$(this).attr('data-image-src-large')+")");
            });
        }

        digitalWall.masonry({
            itemSelector: '.digital-box',
            columnWidth: (width-3*spaceBetweenElements)/4,
            gutterWidth: spaceBetweenElements
        });
        digitalWall.masonry('bindResize');

        lastDigitalWallWidth = width;
        return true;
    }
});
