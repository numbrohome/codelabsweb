class CurrencyInput < SimpleForm::Inputs::Base
  def input
    "#{@builder.text_field(attribute_name, input_html_options.merge({type: :number, pattern: '\d+(\.\d{1,2})?'}))}
      <span class=\"input-currency-code\">#{options[:currency_code]}</span>".html_safe
  end
end