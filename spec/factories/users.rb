require 'faker'

FactoryGirl.define do
  factory :user, class: Public::User do
    email { Faker::Internet.safe_email }
    password 'testtest'
    password_confirmation 'testtest'
    gender Public::User.gender_types(:male)
    birth_date '1988-11-10'
    first_name 'Marcin'
    last_name 'Marcinkowski'
    ip '127.0.0.1'
    role 'admin'
    country_id 'PL'
    status Public::User.status_types(:enabled)
  end
end