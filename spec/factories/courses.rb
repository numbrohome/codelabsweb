FactoryGirl.define do
  factory :course, class: Public::Course do
    title 'Example of course for test purpose.'
    text Faker::Lorem.paragraph(7)
    short_text Faker::Lorem.paragraph(3)
    price 34.29
    status Public::Course.status_types(:regular)
    level Public::Course.level_types(:advanced)
    subject Public::Course.subject_types(:video)
    duration 367
    country { FactoryGirl.create(:country) }
    user { FactoryGirl.create(:user) }
    category { FactoryGirl.create(:category) }
  end
end