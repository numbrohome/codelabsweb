require 'faker'

FactoryGirl.define do
  factory :country, class: Public::Country do
    name { Faker::Address.country }
    code 'us'
    currency_code 'usd'
    currency_symbol '$'
    payment_provider Public::Country.payment_provider_types(:dot_pay)
    tax 20.57
    language 'en'
  end
end