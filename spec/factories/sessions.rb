require 'faker'

FactoryGirl.define do
  factory :session, class: Public::Session do
    skip_create

    text Faker::Lorem.paragraph(7)
    short_text Faker::Lorem.paragraph(3)
    title 'Example of entry for test purpose.'
    status Public::Entry.status_types(:enabled)
    main_display_mode Public::Entry.main_display_mode_types(:medium)
    country { FactoryGirl.create(:country) }
    user { FactoryGirl.create(:user) }
  end
end