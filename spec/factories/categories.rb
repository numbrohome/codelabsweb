FactoryGirl.define do
  factory :category, class: Public::Category do
    name Faker::Lorem.word
  end
end