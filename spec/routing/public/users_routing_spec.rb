require 'spec_helper'

describe Public::UsersController do
  describe 'routing' do

    it 'routes to #new' do
      get('/en/user/register').should route_to(controller: 'public/users', action: 'new', locale: 'en')
    end

    #it "routes to #show" do
    #  get("/admin/countries/1").should route_to("admin/countries#show", :id => "1")
    #end

    #it "routes to #edit" do
    #  get("/admin/countries/1/edit").should route_to("admin/countries#edit", :id => "1")
    #end

    #it "routes to #create" do
    #  post("/admin/countries").should route_to("admin/countries#create")
    #end

    #it "routes to #update" do
    #  put("/admin/countries/1").should route_to("admin/countries#update", :id => "1")
    #end

    #it "routes to #destroy" do
    #  delete("/admin/countries/1").should route_to("admin/countries#destroy", :id => "1")
    #end

  end
end