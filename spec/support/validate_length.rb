module ValidateLength
  class Matcher
    def initialize(attribute)
      @attribute = attribute
    end

    def matches?(model)
      @model = model
      @model.valid?
      errors = @model.errors[@attribute]
      errors.any? { |error| error == @message }
    end

    def failure_message
      "#{@model.class} failed to validate :#{@attribute} presence."
    end
  end

  def validate_presence_of(attribute)
    Matcher.new(attribute)
  end
end