require 'spec_helper'

describe Public::Entry do
  subject(:entry) { FactoryGirl.create(:entry) }

  it 'has a valid factory' do
    entry.should be_valid
  end
end