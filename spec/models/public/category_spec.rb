require 'spec_helper'

describe Public::Category do
  subject(:category) { FactoryGirl.create(:category) }

  it 'has a valid factory' do
    category.should be_valid
  end

  it 'is invalid without name' do
    category.name = nil
    category.should have(2).error_on(:name)
  end

  it 'is invalid when name is too long' do
    category.name = Faker::Lorem.characters(101)
    category.should have(1).error_on(:name)
  end
end