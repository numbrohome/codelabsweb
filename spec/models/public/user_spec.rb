require 'spec_helper'

describe Public::User do
  subject(:user) { FactoryGirl.create(:user) }

  it 'has a valid factory' do
    user.should be_valid
  end

  describe '#website_name' do
    it 'is name of website' do
      user.website = 'http://example.com/'
      user.website_name.should == 'example.com'
    end
  end

  describe 'setting a wrong role' do
    it 'raises an error' do
      expect { user.role = :wrong_role }.to raise_error
    end
  end

  it 'is invalid without email' do
    user.email = nil
    user.should have(2).error_on(:email)
  end

  it 'is invalid when email has wrong format' do
    user.email = 'test@1234512345'
    user.should have(1).error_on(:email)
  end

  it 'is invalid when email it too long' do
    user.email = Faker::Lorem.characters(72)+'@test.com'
    user.should have(1).error_on(:email)
  end

  it 'is invalid when email is not unique' do
    user1 = FactoryGirl.create(:user)
    user2 = FactoryGirl.create(:user)

    user1.email = 'rspec@example.com'
    user2.email = 'rspec@example.com'

    user1.save
    user2.should have(1).error_on(:email)
  end

  it 'is invalid when gender is different than one on the predefined list' do
    user.gender = :wrong_gender
    user.should have(1).error_on(:gender)
  end

  it 'is invalid when status is different than one on the predefined list' do
    user.status = :wrong_status
    user.should have(1).error_on(:status)
  end

  it 'is invalid without birth date' do
    user.birth_date = nil
    user.should have(2).error_on(:birth_date)
  end

  it 'is invalid without birth date is older than 100 years' do
    user.birth_date = Time.now - 101.years
    user.should have(1).error_on(:birth_date)
  end

  it 'is invalid without birth date is younger than 6 years ago' do
    user.birth_date = Time.now - 5.years
    user.should have(1).error_on(:birth_date)
  end

  it 'is invalid when country id is different than one on the predefined list of countries' do
    user.country_id = 'XX'
    user.should have(1).error_on(:country_id)
  end

  it 'is invalid when address is too short' do
    user.address = 'x'
    user.should have(1).error_on(:address)
  end

  it 'is invalid when address is too long' do
    user.address = Faker::Lorem.characters(101)
    user.should have(1).error_on(:address)
  end

  it 'is invalid when location is too short' do
    user.location = 'x'
    user.should have(1).error_on(:location)
  end

  it 'is invalid when location is too long' do
    user.location = Faker::Lorem.characters(101)
    user.should have(1).error_on(:location)
  end

  it 'is invalid when zip code is too short' do
    user.zip_code = 'x'
    user.should have(1).error_on(:zip_code)
  end

  it 'is invalid when zip code is too long' do
    user.zip_code = Faker::Lorem.characters(11)
    user.should have(1).error_on(:zip_code)
  end

  it 'is invalid when first name is too short' do
    user.first_name = 'x'
    user.should have(1).error_on(:first_name)
  end

  it 'is invalid when first name is too long' do
    user.first_name = Faker::Lorem.characters(31)
    user.should have(1).error_on(:first_name)
  end

  it 'is invalid when last name is too short' do
    user.last_name = 'x'
    user.should have(1).error_on(:last_name)
  end

  it 'is invalid when last name is too long' do
    user.last_name = Faker::Lorem.characters(31)
    user.should have(1).error_on(:last_name)
  end

  it 'is invalid when website has a wrong url format' do
    user.website = Faker::Lorem.characters(20)
    user.should have(1).error_on(:website)
  end

  it 'is invalid when website is too short' do
    user.website = Faker::Lorem.characters(9)
    user.should have(2).error_on(:website)
  end

  it 'is invalid when website is too long' do
    user.website = "http://#{Faker::Lorem.characters(89)}.com/"
    user.should have(1).error_on(:website)
  end

  it 'is invalid when phone number is too short' do
    user.phone_number = Faker::Lorem.characters(3)
    user.should have(1).error_on(:phone_number)
  end

  it 'is invalid when phone number is too long' do
    user.phone_number = Faker::Lorem.characters(21)
    user.should have(1).error_on(:phone_number)
  end


  #it 'is invalid without password' do
  #  user.password = nil
  #  user.password_confirmation = nil
  #  user.save
  #  user.should have(1).error_on(:password)
  #end
#
  #it 'is invalid when length of password is less tha 6 characters' do
  #  user.password = '1234'
  #  user.password_confirmation = '1234'
  #  user.should_not be_valid
  #end
#
  #it 'is invalid when password confirmation is different than password' do
  #  user.password = '123456789'
  #  user.password_confirmation = '1234567890'
  #  user.should_not be_valid
  #end


end