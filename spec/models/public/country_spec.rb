require 'spec_helper'

describe Public::Country do
  subject(:country) { FactoryGirl.create(:country) }

  it 'has a valid factory' do
    country.should be_valid
  end

  it 'is invalid without name' do
    country.name = nil
    country.should have(2).error_on(:name)
  end

  it 'is invalid when name it too short' do
    country.name = 'xyz'
    country.should have(1).error_on(:name)
  end

  it 'is invalid when length of currency code is different than 3 characters' do
    country.currency_code = 'xy'
    country.should have(1).error_on(:currency_code)
  end

  it 'is invalid without currency code' do
    country.currency_code = nil
    country.should have(2).error_on(:currency_code)
  end

  it 'is invalid without currency symbol' do
    country.currency_symbol = nil
    country.should have(2).error_on(:currency_symbol)
  end

  it 'is invalid without payment provider' do
    country.payment_provider = nil
    country.should have(2).error_on(:payment_provider)
  end

  it 'is invalid when payment provider is different than one on the predefined list' do
    country.payment_provider = :wrong_provider
    country.should have(1).error_on(:payment_provider)
  end

  it 'is invalid without tax information' do
    country.tax = nil
    country.should have(2).error_on(:tax)
  end

  it 'is invalid when tax is not numeric' do
    country.tax = 'I am not a number'
    country.should have(1).error_on(:tax)
  end

  it 'is invalid when tax is higher than 100' do
    country.tax = 100.01
    country.should have(1).error_on(:tax)
  end

  it 'is invalid when tax is less than 0' do
    country.tax = -0.01
    country.should have(1).error_on(:tax)
  end

  it 'is invalid without language' do
    country.language = nil
    country.should have(2).error_on(:language)
  end

  it 'is invalid when length of language is different than 2 characters' do
    country.language = 'xyz'
    country.should have(1).error_on(:language)
  end
end