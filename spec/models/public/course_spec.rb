require 'spec_helper'

describe Public::Course do
  subject(:course) { FactoryGirl.create(:course) }

  it 'has a valid factory' do
    course.should be_valid
  end

  it 'is invalid without title' do
    course.title = nil
    course.should have(2).error_on(:title)
  end

  it 'is invalid when title is too long' do
    course.title = Faker::Lorem.characters(129)
    course.should have(1).error_on(:title)
  end

  it 'is invalid when title is too short' do
    course.title = Faker::Lorem.characters(4)
    course.should have(1).error_on(:title)
  end

  it 'is valid without text' do
    course.text = nil
    course.should have(0).error_on(:text)
  end

  it 'is invalid when text is too short' do
    course.text = Faker::Lorem.characters(99)
    course.should have(1).error_on(:text)
  end

  it 'is valid without short text' do
    course.short_text = nil
    course.should have(0).error_on(:short_text)
  end

  it 'is invalid when short text is too short' do
    course.short_text = Faker::Lorem.characters(49)
    course.should have(1).error_on(:short_text)
  end
end